# Install ansible

```
sudo apt-get update
sudo apt-get install ansible -y
```

# Run on local Machine
To run playbook on current local Machine:
```
ansible-pull -U https://gitlab.com/jonasasx/lua-ansible.git --connection=local --inventory 127.0.0.1, playbook.yml
```

# Install vagrant

```
sudo apt-get update
sudo apt-get install vagrant virtualbox virtualbox-dkms virtualbox-qt -y
```

# Run on VM
To run playbook on new Virtual Machine:
```
vagrant up
```
